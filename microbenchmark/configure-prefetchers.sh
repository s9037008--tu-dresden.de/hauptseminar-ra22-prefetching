#!/bin/bash

x86a_write -i Intel_HW_Prefetch_Disable -V $1
x86a_write -i Intel_DCU_Prefetch_Disable -V $2
x86a_write -i Intel_AL_Prefetch_Disable -V $3
x86a_write -i Intel_DCU_IP_Prefetch_Disable -V $4
