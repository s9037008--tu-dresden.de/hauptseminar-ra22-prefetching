#include <stdlib.h>
#include <stdio.h>
#include <papi.h>
#include <math.h>

#define CACHESIZE 134217728 // 128 Mi Float = 512 MiB
//#define CACHESIZE 16777216

int main(int argc, char* argv[]) {
	float *a, *b, *c, *cache_filler;
	long long distance, i, size, stride;
	stride = atoi(argv[2]);
	size = atoi(argv[1]) * stride;
	distance = atoi(argv[3]);

	// allocate arrays
	a = (float*) aligned_alloc(64, size*sizeof(float));
	b = (float*) aligned_alloc(64, size*sizeof(float));
	c = (float*) aligned_alloc(64, size*sizeof(float));
	cache_filler = (float*) aligned_alloc(64, CACHESIZE*sizeof(float));

	for (i = 0; i < size; i += stride) {
		a[i] = sqrt((float) i);
		b[i] = 1/sqrt((float) i);
		c[i] = 0;
	}

	// fill cache
	for (i = 0; i < CACHESIZE; i++) {
		cache_filler[i] = 1;
	}

	// initialize PAPI
	int retval, EventSet=PAPI_NULL;
	long long values[4];
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	PAPI_create_eventset(&EventSet);
	PAPI_add_event(EventSet, PAPI_TOT_INS);
	PAPI_add_event(EventSet, PAPI_L3_TCM); // Total Cache Misses
	PAPI_add_event(EventSet, PAPI_L3_LDM); // Load Misses
	PAPI_add_event(EventSet, PAPI_L3_TCA); // Total Accesses
	// start performance counters
	PAPI_start(EventSet);

//	for (i = 0; i < size; i += stride) {
//		__builtin_prefetch(&a[i + 5*stride]);
//		__builtin_prefetch(&b[i + 5*stride]);
//		__builtin_prefetch(&c[i + distance*stride]);
//		c[i] = a[i] * b[i];
//	}

	asm volatile (
			"push %0;" // save size
			"push %1;" // save stride
			"push %2;" // save a
			"push %3;" // save b
			"push %4;" // save c

		"label1:"
			"cmp $0, %0;"
			"jle done;"
			"movss (%2), %%xmm0;"
			"mulss (%3), %%xmm0;"
			"movss %%xmm0, (%4);"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"
			"addss %%xmm0, %%xmm0;"

//			"prefetchnta (%2,%5,8);"
//			"prefetchnta (%3,%5,8);"
//			"prefetchw (%4,%5,8);"
			"add %5, %2;" // -+- add stride to arrays
			"add %5, %3;" //  |
			"add %5, %4;" // -+
			"sub %1, %0;"
			"jmp label1;"

		"done:"
			"pop %4;"
			"pop %3;"
			"pop %2;"
			"pop %1;"
			"pop %0;"

		: // no outputs
		: "c" (size), "r" (stride), "r" (a), "r" (b), "r" (c), "r" (stride * sizeof(float)) // need stride, size, *a, *b and *c as input
		: "memory", "cc", "xmm0" // contents of memory and flag registers will change
	);

	// read performance counters
	PAPI_read(EventSet, values);
	printf("%lld,%lld,%lld\n", values[1], values[2], values[3]);
//	printf("Therefore: %.1f%% Miss rate, %.1f%% of misses were loads\n", (float) 100*values[1]/values[3], (float) 100*values[2]/values[1]);
//	printf("Instructions: %lld\nL3 Total Misses: %lld\nL3 Load Misses: %lld\nL3 Total Accesses: %lld\n", values[0], values[1], values[2], values[3]);

	// be a good boy
	free(a);
	free(b);
	free(c);
	free(cache_filler);

	return 0;
}
