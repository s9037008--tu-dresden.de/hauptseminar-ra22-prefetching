#!/bin/bash


# HW DCU AL IP
./configure-prefetchers.sh 1 1 1 0
make
#echo "HW,DCU,AL,DCU_IP,Stride,Pf-Distance,Total Misses,Load Misses,Total Accesses"

# 1 CL @ 8, d.h. cache miss rates bis dahin sollten alle identisch sein
for i in 1 2 4 8 16 32 64 128 256; do
	echo -n "$i,"
	./traffic 16777216 $i 0
done

#./configure-prefetchers.sh 0 1 1 1


#for i in {0..1}
#do
#	for j in {0..1}
#	do
#		for k in {0..1}
#		do
#			for l in {0..1}
#			do
#				for a in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192
#				do
#					x86a_write -i Intel_HW_Prefetch_Disable -V $i
#					x86a_write -i Intel_DCU_Prefetch_Disable -V $j
#					x86a_write -i Intel_AL_Prefetch_Disable -V $k
#					x86a_write -i Intel_DCU_IP_Prefetch_Disable -V $l
#					echo -n "$i,$j,$k,$l,$a,"
#					./traffic 33554432 $a
#				done
#			done
#		done
#	done
#done
