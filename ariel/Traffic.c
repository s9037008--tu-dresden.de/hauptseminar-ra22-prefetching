#include <stdlib.h>
#include <stdio.h>
#include <papi.h>
#include <math.h>

#define CACHESIZE 8388608 // L3 = 16MiB so 8Mi Floats should suffice
// 134217728 = 2^27 = 128Mi

int main(int argc, char* argv[]) {
	float *a, *b, *c, *cache_filler;
	long i, size, stride;
	size = atoi(argv[1]);
	stride = atoi(argv[2]);

	// allocate arrays
	a = (float*) aligned_alloc(32, size*sizeof(float));
	b = (float*) aligned_alloc(32, size*sizeof(float));
	c = (float*) aligned_alloc(32, size*sizeof(float));
	cache_filler = (float*) aligned_alloc(64, CACHESIZE*sizeof(float));

	for (i = 0; i < size; i++) {
		a[i] = sqrt((float) i);
		b[i] = 1/sqrt((float) i);
		c[i] = 0;
	}

	// fill cache
	for (i = 0; i < CACHESIZE; i++) {
		cache_filler[i] = 1;
	}

	// initialize PAPI
	int retval, EventSet=PAPI_NULL;
	long long values[4];
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	PAPI_create_eventset(&EventSet);
	PAPI_add_event(EventSet, PAPI_TOT_INS);
	PAPI_add_event(EventSet, PAPI_L3_TCM); // Total Cache Misses
	PAPI_add_event(EventSet, PAPI_L3_LDM); // Load Misses
	PAPI_add_event(EventSet, PAPI_L3_TCA); // Total Accesses
	// start performance counters
	PAPI_start(EventSet);

	for (i = 0; i < size; i += stride) {
		c[i] = a[i] * b[i];
	}

	// read performance counters
	PAPI_read(EventSet, values);
	printf("%lld,%lld,%lld,%lld\n", values[0], values[1], values[2], values[3]);
//	printf("Instructions: %lld\nL3 Total Misses: %lld\nL3 Load Misses: %lld\nL3 Total Accesses: %lld\n", values[0], values[1], values[2], values[3]);


	// be a good boi
	free(a);
	free(b);
	free(c);
	free(cache_filler);

	return 0;
}
