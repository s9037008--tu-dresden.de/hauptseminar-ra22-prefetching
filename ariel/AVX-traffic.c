#include <stdlib.h>
#include <stdio.h>
#include <papi.h>

#define CACHESIZE 8388608 // L3 = 16MiB so 8Mi Floats should suffice
#define SIZE 16777216 // 134217728 // 2^27 = 128Mi
#define STRIDE 1

void init_arrays(float *a, float *b, float *c, long size, float* is) {
	float j = 8;
	asm volatile (
			// initialize registers
		"vxorps %%ymm0, %%ymm0, %%ymm0;" // a = 1/sqrt(i)
		"vxorps %%ymm1, %%ymm1, %%ymm1;" // b = sqrt(i)
		"vmovaps (%%rbx), %%ymm2;" //         = i/sqrt(i) -> ymm2 = i..i+7
		"vxorps %%ymm3, %%ymm3, %%ymm3;" // c = 0
		"vbroadcastss (%%rdx), %%ymm4;" // broadcast 8 to ymm4 to add to i..i+7
			// calculate loop iteration count to crunch with AVX (in rax) and remainder to do SISD (in rdx)
		"mov $8, %%rbx;" // 8 floats in ymm
		"xor %%rdx, %%rdx;"
		"div %%rbx;" // rax=(rdx:rax)/rbx, rdx=rax%rbx ... with : concatenation, / division, % modulus

	"mainloop:"
		"vrsqrtps %%ymm2, %%ymm0;"		// 1/sqrt(i) -> a
		"vmovaps %%ymm0, (%%rsi);"		// store a
		"vmulps %%ymm0, %%ymm2, %%ymm1;"	// a*i -> b
		"vmovaps %%ymm1, (%%rdi);"		// store b
		"vmovaps %%ymm3, (%%rcx);"		// 0 -> c
		"vaddps %%ymm2, %%ymm4, %%ymm2;"	// i += 8
		"add $32, %%rcx;" // bytes
		"add $32, %%rsi;" // substitute by vmovaps %%ymm0, -32(%%rsi, %%rax, 8);
		"add $32, %%rdi;"
		"dec %%rax;"
		"cmp $0, %%rax;"
		"jne mainloop;"

	"remainder:"
		"divss %%xmm4, %%xmm4;" // -> xmm4 = 1,8,8,8
		"cmp $0, %%rdx;"
		"je done;"
		"rsqrtss %%xmm2, %%xmm0;"
		"movss %%xmm0, (%%rsi);"
		"mulss %%xmm2, %%xmm0;" // xmm0=sqrt(i), xmm2=i, xmm1=garbage
		"movss %%xmm0, (%%rdi);"
		"movss %%xmm3, (%%rcx);" // move bottom DWORD to c[i]; use SSE to avoid x87
		"addss %%xmm4, %%xmm2;" // i++
		"add $4, %%rcx;"
		"add $4, %%rsi;"
		"add $4, %%rdi;"
		"dec %%rdx;"
		"jmp remainder;"

	"done:"
//		"wbinvd"
		:: "a" (size), "b" (is), "c" (c), "S" (a), "D" (b), "d" (&j)
		: "cc", "memory", "ymm0", "ymm1", "ymm2", "ymm3", "ymm4"
	);
}

int main() {
	float *a, *b, *c, *cache_filler, *is;
	long i;

	// allocate arrays
	a = (float*) aligned_alloc(32, SIZE*sizeof(float));
	b = (float*) aligned_alloc(32, SIZE*sizeof(float));
	c = (float*) aligned_alloc(32, SIZE*sizeof(float));
	cache_filler = (float*) aligned_alloc(64, CACHESIZE*sizeof(float));
	is = (float*) aligned_alloc(32, 8*sizeof(float));

	for (i = 0; i < 8; i++) {
		is[i] = (float) (i);
	}

/*	for (i = 0; i < SIZE; i++) {			// uncomment for validation
		a[i] = 10;
		b[i] = 99;
		c[i] = (float) i;
	} */

	init_arrays(a, b, c, SIZE, is);
/*	init_arrays(a+16, b+16, c+16, SIZE-17, is);	// uncomment for validation
	for (i = 0; i < SIZE; i++)
		printf("%.4f ", a[i]);
	puts("");
	for (i = 0; i < SIZE; i++)
		printf("%.2f ", b[i]);
	puts("");
	for (i = 0; i < SIZE; i++)
		printf("%.0f ", c[i]);
	puts("");
	for (i = 0; i < SIZE; i++)
		printf("%.2f ", a[i]*b[i]);
	puts("");

	// fill cache
	for (i = 0; i < CACHESIZE; i++) {
		cache_filler[i] = 1;
	} */

	// initialize PAPI
	int retval, EventSet=PAPI_NULL;
	long long values[2];
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	PAPI_create_eventset(&EventSet);
	PAPI_add_event(EventSet, PAPI_TOT_INS);
	PAPI_add_event(EventSet, PAPI_L3_TCM); // TCM ?= Total Cache Misses
	// start performance counters
	PAPI_start(EventSet);

	// the longest c[i] = a[i] * b[i] i'ver ever written
	// =SIZE*11/8 + 4 instructions for SIZE%8=0, 16777216 has 704 extra, PAPI has 694 overhead
	// expected L3 misses:
	//	SIZE/8 for each reading a, b and c: SIZE*3/8 in total, 6 291 456 for 16 777 216
	//	none for storing, since c is directly written back to
	asm volatile (
		"vpcmpeqd %%ymm3, %%ymm3, %%ymm3;" // XNOR-substitute. compare ymm3 with itself for equality: ymm3[i] := 0xFFFFFFFF. good mask register.

		"xor %%rdx, %%rdx;"
		"mov $32, %%r14;" // register width in bytes
		"mov $8, %%r15;" // register width in floats
		"imul %%rsi, %%r15;" // stride
		"div %%r15;" // TODO correct with stride?
		"cmp $0, %%rax;"
		"je selectremainder;"

		"cmp $1, %%rsi;" // stride == 1 ? sequential : stride
		"je sequentialloop;"
		"imul %%rsi, %%r14;" // TODO correct?
		"jmp strideloop;"

	"selectremainder:"
		"cmp $1, %%rsi;"
		"je sequentialremainder;"
		"imul %%rsi, %%r14;"
		"jmp strideremainder;"

	"sequentialloop:"
//		"vmovaps (%%rdi), %%ymm0;" // these two are commented out because not strictly neccessary. they generate dram-traffic, tho.
		"vmovaps (%%rbx), %%ymm1;" // no need to vmov (rdi), ymm0 becaues vmul allows one source-operand in memory
//		"vmovaps (%%rcx), %%ymm2;" // ne need to load c[i] because it's overwritten anyway
		"vmulps (%%rdi), %%ymm1, %%ymm2;"
		"vmovaps %%ymm2, (%%rcx);"
		"add %%r14, %%rbx;"
		"add %%r14, %%rcx;"
		"add %%r14, %%rdi;"
		"dec %%rax;"
		"cmp $0, %%rax;"
		"jne sequentialloop;"
//		"jmp sequentialremainder;"

	"sequentialremainder:"
		"cmp $0, %%rdx;"
		"je done1;"
		"movss (%%rdi), %%xmm0;"
		"movss (%%rbx), %%xmm1;"
		"movss (%%rcx), %%xmm2;"
		"mulss %%xmm0, %%xmm1;"
		"movss %%xmm1, %%xmm2;"
		"movss %%xmm2, (%%rcx);"
		"add $4, %%rdi;"
		"add $4, %%rbx;"
		"add $4, %%rcx;"
		"dec %%rdx;"
		"jmp sequentialremainder;"

	"strideloop:"
		"vgatherdps %%ymm3, (%%rdi) , %%ymm0;"
		"vgatherdps %%ymm3, (%%rbx) , %%ymm1;"
		"vmulps %%ymm0, %%ymm1, %%ymm2;"
		scatter %%ymm2 // ohgod there exists no scatter D:
		"add %%r14, %%rbx;"
		"add %%r14, %%rcx;"
		"add %%r14, %%rdi;"
		"dec %%rax;"
		"cmp $0, %%rax;"
		"jne strideloop;"
//		"jmp strideremainder;"

	"strideremainder:"
		//

	"done1:"
		:: "a" (SIZE), "b" (b), "c" (c), "D" (a), "S" (STRIDE)
		: "cc", "memory", "ymm0", "ymm1", "ymm2", "ymm3", "rdx", "r14",  "r15"
	);

	// read performance counters
	PAPI_read(EventSet, values);
	printf("Instructions: %lld\nL3 TCM: %lld\n", values[0], values[1]);

/*	for (i = 0; i < SIZE; i += STRIDE) {
		c[i] = a[i] * b[i];
	} */

	// be a good boi
	free(a);
	free(b);
	free(c);
	free(cache_filler);
	free(is);

	return 0;
}
