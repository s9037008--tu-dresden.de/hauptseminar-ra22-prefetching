import re

def getTimestamps():
	benchsuite = "intrate"
	logfileDir = "/home/s9037008/hauptseminar/SPEC/intrate-no520-allprefetchers/"
	labels = ["0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"]
	filenames = ["CPU2017." + (3-len(str(x)))*"0" + str(x) + ".log" for x in range(2, 18)]
	benchmarks = []
	timestamps = {}
	first = 1

	### Get starting timestamps from a file like out-0000-intrate.log
	for label in labels:
		currentFile = open(logfileDir + "out-" + label + "-" + benchsuite + ".log", "r")
		fileContents = currentFile.read()	# Read file to memory
		timestamps.update({label: {}})		# allocate a key <label>
		if first:							# for <label>, allocate a dictionary with a key for each benchmark
			benchmarks = [x.split(" ")[2] for x in re.findall("Running \(#1\).*", fileContents)]
			first = 0

		for benchmark in benchmarks:
			timestamps.get(label).update({benchmark: []})										# allocate an empty list for each benchmark
			for run in re.findall("Running.*" + benchmark + ".*", fileContents):				# get the run timestamps for all runs of this benchmark
				timestamps.get(label).get(benchmark).append(run.split("[")[1].split("]")[0])	# ...and add it to the list.

		currentFile.close()

	# Now the dictionary looks as such: {'0000': {503.name: ['2022-06-30 19:20:01', ...], 504...}, '0001': ...}
	# Proceed with fetching the corresponding stop timestamps and translate to Epoch time
	# ...except that Epoch is available in the CPU2017.002.log (and similar) files, so just find the right lines (beginning with "  Rate Start" and "  Rate End")

	benchmarks = list(timestamps.get(labels[0]).keys())
	runs = len(timestamps.get(labels[0]).get(benchmarks[0]))

	for i in range(len(filenames)):
		currentFile = open(logfileDir + filenames[i], "r")
		for entry in range(len(benchmarks) * runs):
			if entry < len(benchmarks):
				timestamps.get(labels[i]).update({benchmarks[entry]: []})
			line = currentFile.readline()
			while re.search(".*Rate Start.*", line) == None:
				line = currentFile.readline()
				continue
			startTimeEpoch = line.split("(")[1].split(")")[0]
			stopTimeEpoch = currentFile.readline().split("(")[1].split(")")[0]
			timestamps.get(labels[i]).get(benchmarks[entry % len(benchmarks)]).append((startTimeEpoch, stopTimeEpoch))

#	print(timestamps)
	return timestamps

if __name__ == '__main__':
	getTimestamps()
	exit(0)
