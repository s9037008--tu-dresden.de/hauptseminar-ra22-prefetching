#!/usr/bin/python3

from timestamps import getTimestamps

import asyncio
from time import sleep
from metricq import HistoryClient, Timedelta, Timestamp
from metricq.history_client import HistoryRequestType

async def getHistory(timestamps):
	token = "hauptseminar-kuns"
	url = "amqps://scorep-elab:o70xFIyi&@rabbitmq.metricq.zih.tu-dresden.de"
	client = HistoryClient(token=token, management_url=url)
	await client.connect()

	labels = list(timestamps.keys())
	benchmarks = list(timestamps.get(labels[0]).keys())
	runs = len(timestamps.get(labels[0]).get(benchmarks[0]))
	metrics = {}

	for label in labels:
		metrics.update({label: {}})
		for benchmark in benchmarks:
			metrics.get(label).update({benchmark: {"avgpwr": [], "energy": []}})
			for run in range(runs):
				start = float(timestamps.get(label).get(benchmark)[run][0])
				end = float(timestamps.get(label).get(benchmark)[run][1])
				data = await client.history_aggregate("elab.ariel.power", start_time=Timestamp.from_posix_seconds(start), end_time=Timestamp.from_posix_seconds(end))
				average = data.sum / data.count
				metrics.get(label).get(benchmark).get("avgpwr").append(average)
				metrics.get(label).get(benchmark).get("energy").append(data.integral_s)
			metrics.get(label).get(benchmark).get("avgpwr").sort(reverse=True)
			metrics.get(label).get(benchmark).get("energy").sort(reverse=True)

	return metrics

if __name__ == '__main__':
	timestamps = getTimestamps()
	metrics1 = asyncio.run(getHistory(timestamps))

	metrics = ["avgpwr", "energy"]
	valuesets = ["Maximum", "Median", "Minimum"]

	for metric in metrics:
		outFile = open(metric + "-intrate.csv", "w")
		for i in range(len(valuesets)):
			# Create a header line
			line = valuesets[i]
			for benchmark in metrics1.get(list(metrics1.keys())[0]):
				line += "," + benchmark
			line += "\n"

			outFile.write(line)

			for label in metrics1.keys():
				line = label
				for benchmark in metrics1.get(label).keys():
					line += "," + str(metrics1.get(label).get(benchmark).get(metric)[i])
				line += "\n"
				outFile.write(line)
			outFile.write("\n")
		outFile.close()

	exit(0)
