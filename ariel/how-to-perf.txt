Um perf nutzen zu können, muss es zunächst entsprechende Rechte bekommen:

echo 2 | sudo tee /proc/sys/kernel/perf_event_paranoid

Sodann kann perf angewandt werden. Besonders interessant sind die L3-Misses und PREFETCH-Instruktionen: 
  ls_pref_instr_disp                                
       [Software Prefetch Instructions Dispatched (Speculative)]
  ls_pref_instr_disp.prefetch                       
       [Software Prefetch Instructions Dispatched (Speculative). PrefetchT0, T1 and T2 instructions. See docAPM3 PREFETCHlevel]
  ls_pref_instr_disp.prefetch_nta                   
       [Software Prefetch Instructions Dispatched (Speculative). PrefetchNTA instruction. See docAPM3 PREFETCHlevel]
  ls_pref_instr_disp.prefetch_w                     
       [Software Prefetch Instructions Dispatched (Speculative). PrefetchW instruction. See docAPM3 PREFETCHW]
  ls_sw_pf_dc_fills.ext_cache_local                 
       [Software Prefetch Data Cache Fills by Data Source. From cache of different CCX in same node]
  ls_sw_pf_dc_fills.ext_cache_remote                
       [Software Prefetch Data Cache Fills by Data Source. From CCX Cache in different Node]
  ls_sw_pf_dc_fills.int_cache                       
       [Software Prefetch Data Cache Fills by Data Source. From L3 or different L2 in same CCX]
  ls_sw_pf_dc_fills.lcl_l2                          
       [Software Prefetch Data Cache Fills by Data Source. From Local L2 to the core]
  ls_sw_pf_dc_fills.mem_io_local                    
       [Software Prefetch Data Cache Fills by Data Source. From DRAM or IO connected in same node]
  ls_sw_pf_dc_fills.mem_io_remote                   
       [Software Prefetch Data Cache Fills by Data Source. From DRAM or IO connected in different Node]
  ls_mab_alloc.all_allocations                      
       [All Allocations. Counts when a LS pipe allocates a MAB entry]
  ls_mab_alloc.dc_prefetcher                        
       [LS MAB Allocates by Type. DC prefetcher]
  ls_mab_alloc.hardware_prefetcher_allocations      
       [Hardware Prefetcher Allocations. Counts when a LS pipe allocates a MAB entry]
  ls_mab_alloc.load_store_allocations               
       [Load Store Allocations. Counts when a LS pipe allocates a MAB entry]
  ls_mab_alloc.loads                                
       [LS MAB Allocates by Type. Loads]
  ls_mab_alloc.stores                               
       [LS MAB Allocates by Type. Stores]
  ls_misal_loads.ma4k                               
       [The number of 4KB misaligned (i.e., page crossing) loads]
  ls_misal_loads.ma64                               
       [The number of 64B misaligned (i.e., cacheline crossing) loads]

  ls_dmnd_fills_from_sys.mem_io_local               
       [Demand Data Cache Fills by Data Source. From DRAM or IO connected in same node]
  ls_hw_pf_dc_fills.mem_io_local                    
       [Hardware Prefetch Data Cache Fills by Data Source. From DRAM or IO connected in same node]
  ls_any_fills_from_sys.mem_io_local                
       [Any Data Cache Fills by Data Source. From DRAM or IO connected in same node]
