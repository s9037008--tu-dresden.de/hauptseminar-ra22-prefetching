#!/bin/bash

echo "HW,DCU,AL,DCU_IP,Instructions,Total Misses,Load Misses,Total Accesses"

for i in {0..1}
do
	for j in {0..1}
	do
		for k in {0..1}
		do
			for l in {0..1}
			do
				for a in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536
				do
					x86a_write -i Intel_HW_Prefetch_Disable -V $i
					x86a_write -i Intel_DCU_Prefetch_Disable -V $j
					x86a_write -i Intel_AL_Prefetch_Disable -V $k
					x86a_write -i Intel_DCU_IP_Prefetch_Disable -V $l
					echo -n "$i,$j,$k,$l,$a,"
					./traffic 33554432 $a
				done
			done
		done
	done
done
