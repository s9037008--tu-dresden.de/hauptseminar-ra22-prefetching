#!/bin/bash

RUNLIST="fprate"

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

elab cpu enable
elab ht enable
elab frequency 3000
ml load intel

cd /fastfs/SPEC/cpu
. shrc

for i in {0..1}; do
	for j in {0..1}; do
		for k in {0..1}; do
			for l in {0..1}; do
				x86a_write -i Intel_HW_Prefetch_Disable -V $i
				x86a_write -i Intel_DCU_Prefetch_Disable -V $j
				x86a_write -i Intel_AL_Prefetch_Disable -V $k
				x86a_write -i Intel_DCU_IP_Prefetch_Disable -V $l
				runcpu --config $HOME/hauptseminar/SPEC/cisco-ucs-c240-m5-config.cfg --output_root=$HOME/hauptseminar/SPEC --define default-platform-flags --copies 72 --define smt-on --define cores=36 --define physicalfirst --define invoke_with_interleave --define drop_caches --tune base --output_format cfg,txt --nopower --runmode rate --tune base --size refrate $RUNLIST > $HOME/hauptseminar/SPEC/out-$i$j$k$l-$RUNLIST.log
			done
		done
	done
done

#runcpu --threads 72 --define build_ncpus=72 --config ~/hauptseminar/SPEC/icc.cfg --rebuild --output_root=/home/s9037008/hauptseminar/SPEC all > ~/hauptseminar/SPEC/out-ht.log
#elab ht disable
#runcpu --threads 36 --define build_ncpus=36 --config ~/hauptseminar/SPEC/icc.cfg --output_root=/home/s9037008/hauptseminar/SPEC all > ~/hauptseminar/SPEC/out-noht.log
