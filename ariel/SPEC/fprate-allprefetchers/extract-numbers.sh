#!/bin/bash

for i in {19..34}
do
	tail -n+21 CPU2017.0$i.fprate.refrate.txt | head -n39 | sed 's/\s\+/ /g' | cut -d ' ' -f 4 | paste -sd , -
done
