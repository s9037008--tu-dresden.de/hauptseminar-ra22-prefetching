##################################################################################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
#                                                                                                                                #
# 'reportable' flag not set during run                                                                                           #
#                                                                                                                                #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
##################################################################################################################################
                                            SPEC CPU(R)2017 Floating Point Rate Result
                                                          Cisco Systems
                                             Cisco UCS C240 M5 (Intel Xeon Gold 6154
                                                            3.00 GHz)

                           CPU2017 License: 9019                                    Test date: Jun-2022
                           Test sponsor: Cisco Systems                  Hardware availability: Aug-2017
                           Tested by:    Cisco Systems                  Software availability: Oct-2018

                       Estimated                       Estimated
                 Base     Base        Base        Peak     Peak        Peak
Benchmarks       Copies  Run Time     Rate        Copies  Run Time     Rate 
--------------- -------  ---------  ---------    -------  ---------  ---------
503.bwaves_r         72       1865      387    *
503.bwaves_r         72       1865      387    S
503.bwaves_r         72       1852      390    S
507.cactuBSSN_r      72        599      152    S
507.cactuBSSN_r      72        600      152    S
507.cactuBSSN_r      72        600      152    *
508.namd_r           72        462      148    *
508.namd_r           72        462      148    S
508.namd_r           72        460      149    S
510.parest_r         72       2691       70.0  S
510.parest_r         72       2687       70.1  *
510.parest_r         72       2678       70.3  S
511.povray_r         72        673      250    S
511.povray_r         72        667      252    S
511.povray_r         72        669      251    *
519.lbm_r            72        735      103    S
519.lbm_r            72        732      104    *
519.lbm_r            72        729      104    S
521.wrf_r            72       1066      151    *
521.wrf_r            72       1063      152    S
521.wrf_r            72       1066      151    S
526.blender_r        72        591      186    S
526.blender_r        72        588      186    S
526.blender_r        72        589      186    *
527.cam4_r           72        636      198    S
527.cam4_r           72        634      199    *
527.cam4_r           72        628      200    S
538.imagick_r        72        390      459    S
538.imagick_r        72        390      459    *
538.imagick_r        72        391      458    S
544.nab_r            72        420      289    *
544.nab_r            72        419      289    S
544.nab_r            72        421      288    S
549.fotonik3d_r      72       2325      121    S
549.fotonik3d_r      72       2343      120    S
549.fotonik3d_r      72       2336      120    *
554.roms_r           72       1407       81.3  *
554.roms_r           72       1405       81.4  S
554.roms_r           72       1408       81.3  S
=================================================================================
503.bwaves_r         72       1865      387    *
507.cactuBSSN_r      72        600      152    *
508.namd_r           72        462      148    *
510.parest_r         72       2687       70.1  *
511.povray_r         72        669      251    *
519.lbm_r            72        732      104    *
521.wrf_r            72       1066      151    *
526.blender_r        72        589      186    *
527.cam4_r           72        634      199    *
538.imagick_r        72        390      459    *
544.nab_r            72        420      289    *
549.fotonik3d_r      72       2336      120    *
554.roms_r           72       1407       81.3  *
 Est. SPECrate(R)2017_fp_base             172
 Est. SPECrate(R)2017_fp_peak                                          Not Run


                                                             HARDWARE
                                                             --------
            CPU Name: Intel Xeon Gold 6154
             Max MHz: 3700
             Nominal: 3000
             Enabled: 36 cores, 2 chips, 2 threads/core
           Orderable: 1,2 Chips
            Cache L1: 32 KB I + 32 KB D on chip per core
                  L2: 1 MB I+D on chip per core
                  L3: 24.75 MB I+D on chip per chip
               Other: None
              Memory: 376.578 GB fixme: If using DDR4, the format is:
                      'N GB (N x N GB nRxn PC4-nnnnX-X)'
             Storage: 660 GB  add more disk info here
               Other: None


                                                             SOFTWARE
                                                             --------
                  OS: Ubuntu 18.04.6 LTS
                      5.4.0-53-generic
            Compiler: C/C++: Version 19.0.1.144 of Intel C/C++
                      Compiler for Linux;
                      Fortran: Version 19.0.1.144 of Intel Fortran
                      Compiler for Linux
            Parallel: No
            Firmware: --
         File System: ext4
        System State: Run level 5 (add definition here)
       Base Pointers: 64-bit
       Peak Pointers: Not Applicable
               Other: None
    Power Management: --


                                                           Submit Notes
                                                           ------------
     The numactl mechanism was used to bind copies to processors. The config file option 'submit'
     was used to generate numactl commands to bind each copy to a specific processor.
     For details, please see the config file.

                                                      Operating System Notes
                                                      ----------------------
     Stack size set to unlimited using "ulimit -s unlimited"

                                                          General Notes
                                                          -------------
    Environment variables set by runcpu before the start of the run:
    LD_LIBRARY_PATH = "/home/cpu2017/lib/ia32:/home/cpu2017/lib/intel64"
    
     Binaries compiled on a system with 1x Intel Core i7-4790 CPU + 32GB RAM
     memory using Redhat Enterprise Linux 7.4
     Transparent Huge Pages enabled by default
     Prior to runcpu invocation
     Filesystem page cache synced and cleared with:
     sync; echo 3>       /proc/sys/vm/drop_caches
     runcpu command invoked through numactl i.e.:
     numactl --interleave=all runcpu <etc>
    Yes: The test sponsor attests, as of date of publication, that CVE-2017-5754 (Meltdown)
    is mitigated in the system as tested and documented.
    Yes: The test sponsor attests, as of date of publication, that CVE-2017-5753 (Spectre variant 1)
    is mitigated in the system as tested and documented.
    Yes: The test sponsor attests, as of date of publication, that CVE-2017-5715 (Spectre variant 2)
    is mitigated in the system as tested and documented.

                                                          Platform Notes
                                                          --------------
    BIOS Settings:
    Intel HyperThreading Technology set to Enabled
    CPU performance set to Enterprise
    Power Performance Tuning set to OS Controls
    SNC set to Enabled
    IMC Interleaving set to 1-way Interleave
    Patrol Scrub set to Disabled
    
     Sysinfo program /fastfs/SPEC/cpu/bin/sysinfo
     Rev: r6622 of 2021-04-07 982a61ec0915b55891ef0e16acafc64d
     running on ariel Tue Jun 28 11:18:30 2022
    
     SUT (System Under Test) info as seen by some common utilities.
     For more information on this section, see
        https://www.spec.org/cpu2017/Docs/config.html#sysinfo
    
     From /proc/cpuinfo
        model name : Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz
           2  "physical id"s (chips)
           72 "processors"
        cores, siblings (Caution: counting these is hw and system dependent. The following
        excerpts from /proc/cpuinfo might not be reliable.  Use with caution.)
           cpu cores : 18
           siblings  : 36
           physical 0: cores 0 1 2 3 4 8 9 10 11 16 17 18 19 20 24 25 26 27
           physical 1: cores 0 1 2 3 4 8 9 10 11 16 17 18 19 20 24 25 26 27
    
     From lscpu from util-linux 2.31.1:
          Architecture:        x86_64
          CPU op-mode(s):      32-bit, 64-bit
          Byte Order:          Little Endian
          CPU(s):              72
          On-line CPU(s) list: 0-71
          Thread(s) per core:  2
          Core(s) per socket:  18
          Socket(s):           2
          NUMA node(s):        2
          Vendor ID:           GenuineIntel
          CPU family:          6
          Model:               85
          Model name:          Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz
          Stepping:            4
          CPU MHz:             3000.107
          CPU max MHz:         3001.0000
          CPU min MHz:         1200.0000
          BogoMIPS:            6000.00
          Virtualization:      VT-x
          L1d cache:           32K
          L1i cache:           32K
          L2 cache:            1024K
          L3 cache:            25344K
          NUMA node0 CPU(s):   0-17,36-53
          NUMA node1 CPU(s):   18-35,54-71
          Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov
          pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp
          lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid
          aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16
          xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave
          avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb cat_l3 cdp_l3
          invpcid_single pti intel_ppin ssbd mba ibrs ibpb stibp tpr_shadow vnmi flexpriority
          ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm cqm mpx
          rdt_a avx512f avx512dq rdseed adx smap clflushopt clwb intel_pt avx512cd avx512bw
          avx512vl xsaveopt xsavec xgetbv1 xsaves cqm_llc cqm_occup_llc cqm_mbm_total
          cqm_mbm_local dtherm ida arat pln pts hwp hwp_act_window hwp_epp hwp_pkg_req pku
          ospke md_clear flush_l1d
    
     /proc/cpuinfo cache data
        cache size : 25344 KB
    
     From numactl --hardware
     WARNING: a numactl 'node' might or might not correspond to a physical chip.
       available: 2 nodes (0-1)
       node 0 cpus: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 36 37 38 39 40 41 42 43 44 45
       46 47 48 49 50 51 52 53
       node 0 size: 192089 MB
       node 0 free: 185972 MB
       node 1 cpus: 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 54 55 56 57 58 59 60
       61 62 63 64 65 66 67 68 69 70 71
       node 1 size: 193526 MB
       node 1 free: 185649 MB
       node distances:
       node   0   1
         0:  10  21
         1:  21  10
    
     From /proc/meminfo
        MemTotal:       394870672 kB
        HugePages_Total:       0
        Hugepagesize:       2048 kB
    
     /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor has
        userspace
    
     /usr/bin/lsb_release -d
        Ubuntu 18.04.6 LTS
    
     From /etc/*release* /etc/*version*
        debian_version: buster/sid
        os-release:
           NAME="Ubuntu"
           VERSION="18.04.6 LTS (Bionic Beaver)"
           ID=ubuntu
           ID_LIKE=debian
           PRETTY_NAME="Ubuntu 18.04.6 LTS"
           VERSION_ID="18.04"
           HOME_URL="https://www.ubuntu.com/"
           SUPPORT_URL="https://help.ubuntu.com/"
    
     uname -a:
        Linux ariel 5.4.0-53-generic #59~18.04.1-Ubuntu SMP Wed Oct 21 12:14:56 UTC 2020
        x86_64 x86_64 x86_64 GNU/Linux
    
     Kernel self-reported vulnerability status:
    
     CVE-2018-12207 (iTLB Multihit):                        KVM: Mitigation: Split huge pages
     CVE-2018-3620 (L1 Terminal Fault):                     Mitigation: PTE Inversion; VMX:
                                                            conditional cache flushes, SMT
                                                            vulnerable
     Microarchitectural Data Sampling:                      Mitigation: Clear CPU buffers; SMT
                                                            vulnerable
     CVE-2017-5754 (Meltdown):                              Mitigation: PTI
     CVE-2018-3639 (Speculative Store Bypass):              Mitigation: Speculative Store
                                                            Bypass disabled via prctl and
                                                            seccomp
     CVE-2017-5753 (Spectre variant 1):                     Mitigation: usercopy/swapgs
                                                            barriers and __user pointer
                                                            sanitization
     CVE-2017-5715 (Spectre variant 2):                     Mitigation: Full generic
                                                            retpoline, IBPB: conditional,
                                                            IBRS_FW, STIBP: conditional, RSB
                                                            filling
     CVE-2020-0543 (Special Register Buffer Data Sampling): Not affected
     CVE-2019-11135 (TSX Asynchronous Abort):               Mitigation: Clear CPU buffers; SMT
                                                            vulnerable
    
     run-level 5 Jun 21 20:41
    
     SPEC is set to: /fastfs/SPEC/cpu
        Filesystem     Type  Size  Used Avail Use% Mounted on
        /dev/sda3      ext4  660G  233G  394G  38% /fastfs
    
     From /sys/devices/virtual/dmi/id
         Vendor:         empty
         Product:        empty
         Product Family: empty
    
     Cannot run dmidecode; consider saying (as root)
        chmod +s /usr/sbin/dmidecode
    
     BIOS:
        BIOS Vendor:       American Megatrends Inc.
        BIOS Version:      V1.03
        BIOS Date:         03/01/2018
    
     (End of data from sysinfo program)

                                                      Compiler Version Notes
                                                      ----------------------
    ==============================================================================
    C               | 519.lbm_r(base) 538.imagick_r(base) 544.nab_r(base)
    ------------------------------------------------------------------------------
    icc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    C++             | 508.namd_r(base) 510.parest_r(base)
    ------------------------------------------------------------------------------
    icpc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    C++, C          | 511.povray_r(base) 526.blender_r(base)
    ------------------------------------------------------------------------------
    icpc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    icc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    C++, C, Fortran | 507.cactuBSSN_r(base)
    ------------------------------------------------------------------------------
    icpc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    icc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ifort (IFORT) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    Fortran         | 503.bwaves_r(base) 549.fotonik3d_r(base) 554.roms_r(base)
    ------------------------------------------------------------------------------
    ifort (IFORT) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    Fortran, C      | 521.wrf_r(base) 527.cam4_r(base)
    ------------------------------------------------------------------------------
    ifort (IFORT) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    icc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------

                                                     Base Compiler Invocation
                                                     ------------------------
C benchmarks: 
     icc -m64 -std=c11

C++ benchmarks: 
     icpc -m64

Fortran benchmarks: 
     ifort -m64

Benchmarks using both Fortran and C: 
     ifort -m64 icc -m64 -std=c11

Benchmarks using both C and C++: 
     icpc -m64 icc -m64 -std=c11

Benchmarks using Fortran, C, and C++: 
     icpc -m64 icc -m64 -std=c11 ifort -m64


                                                      Base Portability Flags
                                                      ----------------------
    503.bwaves_r: -DSPEC_LP64
 507.cactuBSSN_r: -DSPEC_LP64
      508.namd_r: -DSPEC_LP64
    510.parest_r: -DSPEC_LP64
    511.povray_r: -DSPEC_LP64
       519.lbm_r: -DSPEC_LP64
       521.wrf_r: -DSPEC_LP64 -DSPEC_CASE_FLAG -convert big_endian
   526.blender_r: -DSPEC_LP64 -DSPEC_LINUX -funsigned-char
      527.cam4_r: -DSPEC_LP64 -DSPEC_CASE_FLAG
   538.imagick_r: -DSPEC_LP64
       544.nab_r: -DSPEC_LP64
 549.fotonik3d_r: -DSPEC_LP64
      554.roms_r: -DSPEC_LP64


                                                     Base Optimization Flags
                                                     -----------------------
C benchmarks: 
     -xCORE-AVX512 -ipo -O3 -no-prec-div -qopt-prefetch -ffinite-math-only -qopt-mem-layout-trans=3

C++ benchmarks: 
     -xCORE-AVX512 -ipo -O3 -no-prec-div -qopt-prefetch -ffinite-math-only -qopt-mem-layout-trans=3

Fortran benchmarks: 
     -xCORE-AVX512 -ipo -O3 -no-prec-div -qopt-prefetch -ffinite-math-only -qopt-mem-layout-trans=3 -nostandard-realloc-lhs
     -align array32byte

Benchmarks using both Fortran and C: 
     -xCORE-AVX512 -ipo -O3 -no-prec-div -qopt-prefetch -ffinite-math-only -qopt-mem-layout-trans=3 -nostandard-realloc-lhs
     -align array32byte

Benchmarks using both C and C++: 
     -xCORE-AVX512 -ipo -O3 -no-prec-div -qopt-prefetch -ffinite-math-only -qopt-mem-layout-trans=3

Benchmarks using Fortran, C, and C++: 
     -xCORE-AVX512 -ipo -O3 -no-prec-div -qopt-prefetch -ffinite-math-only -qopt-mem-layout-trans=3 -nostandard-realloc-lhs
     -align array32byte


  SPEC CPU and SPECrate are registered trademarks of the Standard Performance Evaluation Corporation.  All other brand and
    product names appearing in this result are trademarks or registered trademarks of their respective holders.
##################################################################################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
#                                                                                                                                #
# 'reportable' flag not set during run                                                                                           #
#                                                                                                                                #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
##################################################################################################################################
----------------------------------------------------------------------------------------------------------------------------------
For questions about this result, please contact the tester.
For other inquiries, please contact info@spec.org.
Copyright 2017-2022 Standard Performance Evaluation Corporation
Tested with SPEC CPU(R)2017 v1.1.8 on 2022-06-28 11:18:30+0200.
Report generated on 2022-06-28 22:57:03 by CPU2017 text formatter v6255.
