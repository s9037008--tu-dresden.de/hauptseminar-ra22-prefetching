##################################################################################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
#                                                                                                                                #
# 'reportable' flag not set during run                                                                                           #
#                                                                                                                                #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
##################################################################################################################################
                                               SPEC CPU(R)2017 Integer Rate Result
                                                             TYAN --
                                                     Test Sponsor: TU Dresden

                           CPU2017 License: 13                                      Test date: Jun-2022
                           Test sponsor: TU Dresden                     Hardware availability:         
                           Tested by:    TU Dresden                     Software availability:         

                       Estimated                       Estimated
                 Base     Base        Base        Peak     Peak        Peak
Benchmarks       Copies  Run Time     Rate        Copies  Run Time     Rate 
--------------- -------  ---------  ---------    -------  ---------  ---------
500.perlbench_r       1        363       4.38  *
500.perlbench_r       1        363       4.39  S
500.perlbench_r       1        365       4.36  S
502.gcc_r             1        283       5.00  *
502.gcc_r             1        283       5.00  S
502.gcc_r             1        283       5.00  S
505.mcf_r             1        319       5.07  S
505.mcf_r             1        319       5.07  *
505.mcf_r             1        320       5.06  S
520.omnetpp_r         1        405       3.24  S
520.omnetpp_r         1        405       3.24  *
520.omnetpp_r         1        406       3.23  S
523.xalancbmk_r       1        244       4.33  S
523.xalancbmk_r       1        243       4.34  *
523.xalancbmk_r       1        243       4.34  S
525.x264_r            1        165      10.6   S
525.x264_r            1        164      10.6   *
525.x264_r            1        164      10.6   S
531.deepsjeng_r       1        293       3.91  S
531.deepsjeng_r       1        293       3.91  S
531.deepsjeng_r       1        293       3.91  *
541.leela_r           1        487       3.40  S
541.leela_r           1        487       3.40  *
541.leela_r           1        487       3.40  S
548.exchange2_r       1        300       8.74  S
548.exchange2_r       1        300       8.74  *
548.exchange2_r       1        302       8.69  S
557.xz_r              1        366       2.95  S
557.xz_r              1        366       2.95  S
557.xz_r              1        366       2.95  *
=================================================================================
500.perlbench_r       1        363       4.38  *
502.gcc_r             1        283       5.00  *
505.mcf_r             1        319       5.07  *
520.omnetpp_r         1        405       3.24  *
523.xalancbmk_r       1        243       4.34  *
525.x264_r            1        164      10.6   *
531.deepsjeng_r       1        293       3.91  *
541.leela_r           1        487       3.40  *
548.exchange2_r       1        300       8.74  *
557.xz_r              1        366       2.95  *
 Est. SPECrate(R)2017_int_base           4.74
 Est. SPECrate(R)2017_int_peak                                         Not Run


                                                             HARDWARE
                                                             --------
            CPU Name: Intel Xeon Gold 6154
             Max MHz:  
             Nominal:  
             Enabled:  cores, 2 chips,  threads/core
           Orderable:  
            Cache L1:  
                  L2:  
                  L3:  
               Other: --
              Memory:  
                      'N GB (N x N GB nRxn PC4-nnnnX-X)'
             Storage: 660 GB  add more disk info here
               Other: --


                                                             SOFTWARE
                                                             --------
                  OS: Ubuntu 18.04.6 LTS
                      5.4.0-53-generic
            Compiler: C/C++: Version 19.0.0.117 20180804 of Intel C/C++
                      Compiler for Linux;
                      Fortran: Versionn 19.0.0.117 20180804 of Intel
                      Fortran
                      Compiler for Linux
            Parallel: No
            Firmware:  
         File System: ext4
        System State: Run level 5 (add definition here)
       Base Pointers: 64-bit
       Peak Pointers: Not Applicable
               Other: --
    Power Management:  


                                                           Submit Notes
                                                           ------------
    The config file option 'submit' was used.

                                                          Platform Notes
                                                          --------------
    
     Sysinfo program /fastfs/SPEC/cpu/bin/sysinfo
     Rev: r6622 of 2021-04-07 982a61ec0915b55891ef0e16acafc64d
     running on ariel Thu Jun  9 00:29:17 2022
    
     SUT (System Under Test) info as seen by some common utilities.
     For more information on this section, see
        https://www.spec.org/cpu2017/Docs/config.html#sysinfo
    
     From /proc/cpuinfo
        model name : Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz
           2  "physical id"s (chips)
           72 "processors"
        cores, siblings (Caution: counting these is hw and system dependent. The following
        excerpts from /proc/cpuinfo might not be reliable.  Use with caution.)
           cpu cores : 18
           siblings  : 36
           physical 0: cores 0 1 2 3 4 8 9 10 11 16 17 18 19 20 24 25 26 27
           physical 1: cores 0 1 2 3 4 8 9 10 11 16 17 18 19 20 24 25 26 27
    
     From lscpu from util-linux 2.31.1:
          Architecture:        x86_64
          CPU op-mode(s):      32-bit, 64-bit
          Byte Order:          Little Endian
          CPU(s):              72
          On-line CPU(s) list: 0-71
          Thread(s) per core:  2
          Core(s) per socket:  18
          Socket(s):           2
          NUMA node(s):        2
          Vendor ID:           GenuineIntel
          CPU family:          6
          Model:               85
          Model name:          Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz
          Stepping:            4
          CPU MHz:             1486.658
          CPU max MHz:         3001.0000
          CPU min MHz:         1200.0000
          BogoMIPS:            6000.00
          Virtualization:      VT-x
          L1d cache:           32K
          L1i cache:           32K
          L2 cache:            1024K
          L3 cache:            25344K
          NUMA node0 CPU(s):   0-17,36-53
          NUMA node1 CPU(s):   18-35,54-71
          Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov
          pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp
          lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid
          aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16
          xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave
          avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb cat_l3 cdp_l3
          invpcid_single pti intel_ppin ssbd mba ibrs ibpb stibp tpr_shadow vnmi flexpriority
          ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm cqm mpx
          rdt_a avx512f avx512dq rdseed adx smap clflushopt clwb intel_pt avx512cd avx512bw
          avx512vl xsaveopt xsavec xgetbv1 xsaves cqm_llc cqm_occup_llc cqm_mbm_total
          cqm_mbm_local dtherm ida arat pln pts hwp hwp_act_window hwp_epp hwp_pkg_req pku
          ospke md_clear flush_l1d
    
     /proc/cpuinfo cache data
        cache size : 25344 KB
    
     From numactl --hardware
     WARNING: a numactl 'node' might or might not correspond to a physical chip.
       available: 2 nodes (0-1)
       node 0 cpus: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 36 37 38 39 40 41 42 43 44 45
       46 47 48 49 50 51 52 53
       node 0 size: 192089 MB
       node 0 free: 187981 MB
       node 1 cpus: 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 54 55 56 57 58 59 60
       61 62 63 64 65 66 67 68 69 70 71
       node 1 size: 193526 MB
       node 1 free: 160712 MB
       node distances:
       node   0   1
         0:  10  21
         1:  21  10
    
     From /proc/meminfo
        MemTotal:       394870672 kB
        HugePages_Total:       0
        Hugepagesize:       2048 kB
    
     /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor has
        userspace
    
     /usr/bin/lsb_release -d
        Ubuntu 18.04.6 LTS
    
     From /etc/*release* /etc/*version*
        debian_version: buster/sid
        os-release:
           NAME="Ubuntu"
           VERSION="18.04.6 LTS (Bionic Beaver)"
           ID=ubuntu
           ID_LIKE=debian
           PRETTY_NAME="Ubuntu 18.04.6 LTS"
           VERSION_ID="18.04"
           HOME_URL="https://www.ubuntu.com/"
           SUPPORT_URL="https://help.ubuntu.com/"
    
     uname -a:
        Linux ariel 5.4.0-53-generic #59~18.04.1-Ubuntu SMP Wed Oct 21 12:14:56 UTC 2020
        x86_64 x86_64 x86_64 GNU/Linux
    
     Kernel self-reported vulnerability status:
    
     CVE-2018-12207 (iTLB Multihit):                        KVM: Mitigation: Split huge pages
     CVE-2018-3620 (L1 Terminal Fault):                     Mitigation: PTE Inversion; VMX:
                                                            conditional cache flushes, SMT
                                                            vulnerable
     Microarchitectural Data Sampling:                      Mitigation: Clear CPU buffers; SMT
                                                            vulnerable
     CVE-2017-5754 (Meltdown):                              Mitigation: PTI
     CVE-2018-3639 (Speculative Store Bypass):              Mitigation: Speculative Store
                                                            Bypass disabled via prctl and
                                                            seccomp
     CVE-2017-5753 (Spectre variant 1):                     Mitigation: usercopy/swapgs
                                                            barriers and __user pointer
                                                            sanitization
     CVE-2017-5715 (Spectre variant 2):                     Mitigation: Full generic
                                                            retpoline, IBPB: conditional,
                                                            IBRS_FW, STIBP: conditional, RSB
                                                            filling
     CVE-2020-0543 (Special Register Buffer Data Sampling): Not affected
     CVE-2019-11135 (TSX Asynchronous Abort):               Mitigation: Clear CPU buffers; SMT
                                                            vulnerable
    
     run-level 5 Jun 7 10:22
    
     SPEC is set to: /fastfs/SPEC/cpu
        Filesystem     Type  Size  Used Avail Use% Mounted on
        /dev/sda3      ext4  660G  233G  394G  38% /fastfs
    
     From /sys/devices/virtual/dmi/id
         Vendor:         empty
         Product:        empty
         Product Family: empty
    
     Cannot run dmidecode; consider saying (as root)
        chmod +s /usr/sbin/dmidecode
    
     BIOS:
        BIOS Vendor:       American Megatrends Inc.
        BIOS Version:      V1.03
        BIOS Date:         03/01/2018
    
     (End of data from sysinfo program)

                                                      Compiler Version Notes
                                                      ----------------------
    ==============================================================================
    C       | 500.perlbench_r(base) 502.gcc_r(base) 505.mcf_r(base)
            | 525.x264_r(base) 557.xz_r(base)
    ------------------------------------------------------------------------------
    icc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    C++     | 520.omnetpp_r(base) 523.xalancbmk_r(base) 531.deepsjeng_r(base)
            | 541.leela_r(base)
    ------------------------------------------------------------------------------
    icpc (ICC) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------
    
    ==============================================================================
    Fortran | 548.exchange2_r(base)
    ------------------------------------------------------------------------------
    ifort (IFORT) 19.0.0.117 20180804
    Copyright (C) 1985-2018 Intel Corporation.  All rights reserved.
    ------------------------------------------------------------------------------

                                                     Base Compiler Invocation
                                                     ------------------------
C benchmarks: 
     icc

C++ benchmarks: 
     icpc

Fortran benchmarks: 
     ifort


                                                      Base Portability Flags
                                                      ----------------------
 500.perlbench_r: -DSPEC_LP64 -DSPEC_LINUX_X64
       502.gcc_r: -DSPEC_LP64
       505.mcf_r: -DSPEC_LP64
   520.omnetpp_r: -DSPEC_LP64
 523.xalancbmk_r: -DSPEC_LP64 -DSPEC_LINUX
      525.x264_r: -DSPEC_LP64
 531.deepsjeng_r: -DSPEC_LP64
     541.leela_r: -DSPEC_LP64
 548.exchange2_r: -DSPEC_LP64
        557.xz_r: -DSPEC_LP64


                                                     Base Optimization Flags
                                                     -----------------------
C benchmarks: 
     -m64 -std=c11 -g -xHOST -ipo -O3 -no-prec-div -qopt-mem-layout-trans=3 -qopt-prefetch

C++ benchmarks: 
     -m64 -g -xHOST -ipo -O3 -no-prec-div -qopt-mem-layout-trans=3 -qopt-prefetch

Fortran benchmarks: 
     -m64 -g -xHOST -ipo -O3 -no-prec-div -qopt-mem-layout-trans=3 -qopt-prefetch -nostandard-realloc-lhs


  SPEC CPU and SPECrate are registered trademarks of the Standard Performance Evaluation Corporation.  All other brand and
    product names appearing in this result are trademarks or registered trademarks of their respective holders.
##################################################################################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
#                                                                                                                                #
# 'reportable' flag not set during run                                                                                           #
#                                                                                                                                #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN                                                      #
##################################################################################################################################
----------------------------------------------------------------------------------------------------------------------------------
For questions about this result, please contact the tester.
For other inquiries, please contact info@spec.org.
Copyright 2017-2022 Standard Performance Evaluation Corporation
Tested with SPEC CPU(R)2017 v1.1.8 on 2022-06-09 00:29:16+0200.
Report generated on 2022-06-09 08:55:52 by CPU2017 text formatter v6255.
