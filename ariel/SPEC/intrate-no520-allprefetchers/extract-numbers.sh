#!/bin/bash

for i in {002..017}
do
	tail -n+25 CPU2017.$i.intrate.refrate.txt | head -n28 | sed 's/\s\+/ /g' | cut -d ' ' -f 4 | paste -sd , -
done
